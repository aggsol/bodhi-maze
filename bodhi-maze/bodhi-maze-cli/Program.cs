using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bodhi.Data;
using bodhi.Exporter;
using bodhi.Generator;

namespace bodhi
{
    class Program
    {
        static void Main(string[] args)
        {
            Grid grid = new Grid(16, 16, 16);
            //MaskedGrid grid = new MaskedGrid("Masks/mask04.png", 1);
            //BinaryTree.RunSouthWest(grid);
            //BinaryTree.RunNorthEast(grid);
            //SideWinder.Run(grid);
            //AldousBroder.Run(grid);
            //Wilson.Run(grid);
            //HuntAndKill.Run(grid);
            RecursiveBacktracker.Run(grid);

            Console.WriteLine("Calculate distances...");
            //var dist = new Distances(grid.GetCell(grid.Width / 2, grid.Height / 2));
            var dist = new Distances(grid.GetRandomCell());
            dist.CalculateDistances();

            int max;
            var farthed = dist.GetFarthestCell(out max);
            var deadends = grid.Deadends;
            Console.WriteLine("Deadends: {0} Max Path: {1}", deadends.Count, max);

            if (grid.Height <= 16 && grid.Width <= 16)
            {
                TextPrinter.WriteToConsole(grid, dist);
                TextPrinter.WriteToConsole(grid, dist.FindPathTo(grid.GetCell(grid.Width - 1, 0)));
                TextPrinter.WriteToConsole(grid);
            }

            PngWriter writer = new PngWriter();
            var filename = string.Format("maze-{0}-{1}x{2}-({3}).png", grid.Generator, grid.Width, grid.Height, deadends.Count);
            writer.WritePng(grid, filename, dist);
            Console.WriteLine("Created file: {0}", filename);

            TmxWriter tmxWriter = new TmxWriter(grid);
            tmxWriter.Write("example.tmx");

            Console.WriteLine("Press ENTER to exit...");
            Console.ReadLine();
        }


    }
}
