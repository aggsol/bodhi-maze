﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bodhi
{
    class TextPrinter
    {
        public static void WriteToConsole(Grid grid, Distances dist)
        {
            Console.Write("+");
            for (int x = 0; x < grid.Width; ++x)
            {
                Console.Write("----+");
            }
            Console.Write("\n");

            for (int y = grid.Height - 1; y >= 0; --y)
            {
                string top = "|";
                string bottom = "+";
                for (int x = 0; x < grid.Width; ++x)
                {
                    var cell = grid.GetCell(x, y);
                    int val = dist.GetDistance(cell);
                    if(val >= 0)
                        top += " " + val.ToString("X2") + " ";
                    else
                        top += "    ";

                    if (cell.IsLinkedWith(cell.East) == false) top += "|";
                    else top += " ";

                    if (cell.IsLinkedWith(cell.South) == false) bottom += "----";
                    else bottom += "    ";
                    bottom += "+";
                }
                Console.WriteLine(top);
                Console.WriteLine(bottom);
            }
        }

        public static void WriteToConsole(Grid grid)
        {
            Console.Write("+");
            for (int x = 0; x < grid.Width; ++x)
            {
                Console.Write("---+");
            }
            Console.Write("\n");

            for (int y = grid.Height - 1; y >= 0; --y)
            {
                string top = "|";
                string bottom = "+";
                for (int x = 0; x < grid.Width; ++x)
                {
                    var cell = grid.GetCell(x, y);
                    top += "   ";
                    if (cell.IsLinkedWith(cell.East) == false) top += "|";
                    else top += " ";

                    if (cell.IsLinkedWith(cell.South) == false) bottom += "---";
                    else bottom += "   ";
                    bottom += "+";
                }
                Console.WriteLine(top);
                Console.WriteLine(bottom);
            }
        }
    }
}
