﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bodhi.Exporter
{
    public static class TileTypes
    {
        public enum SingleWallTile
        {
            None = 0,
            Fill = 1,
            Single = 2,
            WestEastWalls = 3,
            NorthSouthWalls = 4,
            WestWall = 5,
            NorthWall = 6,
            EastWall = 7,
            SouthWall = 8,
            NorthWestCorner = 9,
            NorthEastCorner = 10,
            SouthEastCorner = 11,
            SouthWestCorner = 12,
            WestCap = 13,
            NorthCap = 14,
            EastCap = 15,
            SouthCap = 16
        }

        [Flags]
        public enum Wall
        {
            None = 0,
            North = 1,
            South = 2,
            East = 4,
            West = 8
        }
    }
}
