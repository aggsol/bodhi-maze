﻿using System.Text;
using System.Xml;

namespace bodhi.Exporter
{
    public class TmxWriter
    {
        private readonly Grid m_grid;
        private readonly TmxWriterSettings m_settings;
        private readonly TileTypes.SingleWallTile[,] m_tiles;

        public TmxWriter(Grid grid)
        {
            m_grid = grid;
            m_settings = new TmxWriterSettings();
            m_settings.TileHeight = 64;
            m_settings.TileWidth = 64;
            m_settings.Width = (grid.Width*2) + 1;
            m_settings.Height = (grid.Height*2) + 1;

            m_tiles = new TileTypes.SingleWallTile[m_settings.Width, m_settings.Height];

            CarvePath();

            SetupTiles();
        }

        private void SetupTiles()
        {
            for (var y = 0; y < m_settings.Height; y += 1)
            {
                for (var x = 0; x < m_settings.Width; x += 1)
                {
                    if (m_tiles[x, y] == TileTypes.SingleWallTile.None) continue;

                    var walls = TileTypes.Wall.None;

                    if (y < m_settings.Height - 1)
                    {
                        if (m_tiles[x, y + 1] == TileTypes.SingleWallTile.None)
                        {
                            walls |= TileTypes.Wall.North;
                        }
                    }

                    if (y > 0)
                    {
                        if (m_tiles[x, y - 1] == TileTypes.SingleWallTile.None)
                        {
                            walls |= TileTypes.Wall.South;
                        }
                    }

                    if (x < m_settings.Width - 1)
                    {
                        if (m_tiles[x + 1, y] == TileTypes.SingleWallTile.None)
                        {
                            walls |= TileTypes.Wall.East;
                        }
                    }

                    if (x > 0)
                    {
                        if (m_tiles[x - 1, y] == TileTypes.SingleWallTile.None)
                        {
                            walls |= TileTypes.Wall.West;
                        }
                    }

                    switch ((int) walls)
                    {
                        case 1:
                            m_tiles[x, y] = TileTypes.SingleWallTile.NorthWall;
                            break;
                        case 2:
                            m_tiles[x, y] = TileTypes.SingleWallTile.SouthWall;
                            break;
                        case 3:
                            m_tiles[x, y] = TileTypes.SingleWallTile.NorthSouthWalls;
                            break;
                        case 4:
                            m_tiles[x, y] = TileTypes.SingleWallTile.EastWall;
                            break;
                        case 5:
                            m_tiles[x, y] = TileTypes.SingleWallTile.NorthEastCorner;
                            break;
                        case 6:
                            m_tiles[x, y] = TileTypes.SingleWallTile.SouthEastCorner;
                            break;
                        case 7:
                            m_tiles[x, y] = TileTypes.SingleWallTile.EastCap;
                            break;
                        case 8:
                            m_tiles[x, y] = TileTypes.SingleWallTile.WestWall;
                            break;
                        case 9:
                            m_tiles[x, y] = TileTypes.SingleWallTile.NorthWestCorner;
                            break;
                        case 10:
                            m_tiles[x, y] = TileTypes.SingleWallTile.SouthWestCorner;
                            break;
                        case 11:
                            m_tiles[x, y] = TileTypes.SingleWallTile.WestCap;
                            break;
                        case 12:
                            m_tiles[x, y] = TileTypes.SingleWallTile.WestEastWalls;
                            break;
                        case 13:
                            m_tiles[x, y] = TileTypes.SingleWallTile.NorthCap;
                            break;
                        case 14:
                            m_tiles[x, y] = TileTypes.SingleWallTile.SouthCap;
                            break;
                        case 15:
                            m_tiles[x, y] = TileTypes.SingleWallTile.Single;
                            break;
                    }
                }
            }
        }

        private void CarvePath()
        {
            for (var y = 0; y < m_settings.Height; y += 1)
            {
                for (var x = 0; x < m_settings.Width; x += 1)
                {
                    m_tiles[x, y] = TileTypes.SingleWallTile.Fill;
                }
            }

            foreach (Cell cell in m_grid.EachCell())
            {
                if (cell.IsLinked)
                {
                    var tileX = (cell.X*2) + 1;
                    var tileY = (cell.Y*2) + 1;

                    m_tiles[tileX, tileY] = TileTypes.SingleWallTile.None;

                    foreach (var link in cell.Links)
                    {
                        var offsetX = link.X - cell.X;
                        var offsetY = link.Y - cell.Y;

                        m_tiles[tileX + offsetX, tileY + offsetY] = TileTypes.SingleWallTile.None;
                    }
                }
            }
        }

        public void Write(string path)
        {
            var settings = new XmlWriterSettings();
            settings.Encoding = Encoding.UTF8;
            settings.Indent = true;
            settings.NewLineOnAttributes = false;

            using (var writer = XmlWriter.Create(path, settings))
            {
                writer.WriteStartDocument();

                StartMapTag(writer);

                AddTileSet(writer);

                writer.WriteStartElement("layer");
                writer.WriteAttributeString("name", "mazewalls");
                writer.WriteAttributeString("width", m_settings.Width.ToString());
                writer.WriteAttributeString("height", m_settings.Height.ToString());

                writer.WriteStartElement("data");

                for (var y = m_settings.Height - 1; y >= 0; y -= 1)
                {
                    for (var x = 0; x < m_settings.Width; x += 1)
                    {
                        writer.WriteStartElement("tile");
                        writer.WriteAttributeString("gid", ((int) (m_tiles[x, y])).ToString());
                        writer.WriteEndElement();
                    }
                }

                writer.WriteEndElement(); // end layer

                writer.WriteEndElement(); // end map

                writer.WriteEndDocument();
            }
        }

        private void AddTileSet(XmlWriter writer)
        {
            writer.WriteStartElement("tileset");
            writer.WriteAttributeString("firstgid", "1");
            writer.WriteAttributeString("name", "BaseTiles");
            writer.WriteAttributeString("tilewidth", "64");
            writer.WriteAttributeString("tileheight", "64");
            writer.WriteAttributeString("tilecount", "16");

            writer.WriteStartElement("image");
            writer.WriteAttributeString("source", "TileSets/BaseTiles.png");
            writer.WriteAttributeString("width", "256");
            writer.WriteAttributeString("height", "256");

            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        private void StartMapTag(XmlWriter writer)
        {
            writer.WriteStartElement("map");
            writer.WriteAttributeString("orientation", "orthogonal");
            writer.WriteAttributeString("renderorder", "right-down");
            writer.WriteAttributeString("width", m_settings.Width.ToString());
            writer.WriteAttributeString("height", m_settings.Height.ToString());
            writer.WriteAttributeString("tilewidth", m_settings.TileWidth.ToString());
            writer.WriteAttributeString("tileheight", m_settings.TileHeight.ToString());
        }

        public class TmxWriterSettings
        {
            public int TileWidth { get; set; }
            public int TileHeight { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
        }
    }
}