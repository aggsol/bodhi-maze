using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;

namespace bodhi.Exporter
{
    public class PngWriter
    {
        private readonly int m_cellSize;

        public PngWriter(int cellSize = 16)
        {
            m_cellSize = cellSize;
        }

        public void WritePng(Grid grid, string filename, Distances distances)
        {
            var maximum = 0;
            if (distances != null)
            {
                distances.GetFarthestCell(out maximum);
            }

            var width = grid.Width*m_cellSize;
            var height = grid.Height*m_cellSize;

            var wallBrush = new SolidBrush(Color.DarkSlateGray);
            var wall = new Pen(wallBrush, 2.0f);

            using (var bitmap = new Bitmap(width, height))
            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.Clear(Color.DeepPink);

                for (var y = 0; y < grid.Height; ++y)
                {
                    var y1 = height - (y*m_cellSize);
                    var y2 = height - ((y + 1)*m_cellSize);

                    for (var x = 0; x < grid.Width; ++x)
                    {
                        var cell = grid.GetCell(x, y);
                        var x1 = x * m_cellSize;
                        var x2 = (x + 1) * m_cellSize;

                        if (cell == null)
                        {
                            var rect = new Rectangle(x1, y2, m_cellSize, m_cellSize);
                            graphics.FillRectangle(wallBrush, rect);
                            continue;
                        }

                        if (distances != null)
                        {
                            var value = distances.GetDistance(cell);
                            var intensity = (maximum - value)/(float) maximum;

                            Debug.Assert(intensity >= 0.0f);
                            Debug.Assert(intensity <= 1.0f);

                            var dark = (int) Math.Round(128f*intensity);
                            var bright = 127 + (int) Math.Round(128f*intensity);

                            var col = Color.FromArgb(255, dark, bright, bright);
                            var fillBrush = new SolidBrush(col);
                            var rect = new Rectangle(x1, y2, m_cellSize, m_cellSize);
                            graphics.FillRectangle(fillBrush, rect);
                        }

                        var lowerLeft = new Point(x1, y1);
                        var lowerRight = new Point(x2, y1);
                        var upperLeft = new Point(x1, y2);
                        var upperRight = new Point(x2, y2);

                        if (cell.IsLinkedWith(cell.North) == false) graphics.DrawLine(wall, upperLeft, upperRight);
                        if (cell.IsLinkedWith(cell.South) == false) graphics.DrawLine(wall, lowerLeft, lowerRight);
                        if (cell.IsLinkedWith(cell.East) == false) graphics.DrawLine(wall, upperRight, lowerRight);
                        if (cell.IsLinkedWith(cell.West) == false) graphics.DrawLine(wall, upperLeft, lowerLeft);
                    }
                }
                bitmap.Save(filename, ImageFormat.Png);
            }
        }
    }
}