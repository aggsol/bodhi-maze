﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bodhi
{
    public static class Sample
    {
        /// <summary>
        /// Get a random element from a List
        /// </summary>
        /// <typeparam name="T">List item type</typeparam>
        /// <param name="self">The container</param>
        /// <param name="random">Source of entropy</param>
        /// <returns>Random sample from list</returns>
        public static T GetRandomSample<T>(this List<T> self, Random random)
        {
            if(random == null) random = new Random();
            if (self.Count == 0)
            {
                throw new ArgumentOutOfRangeException("list is empty");
            }
            return self[random.Next(0, self.Count)];
        }
    }
}
