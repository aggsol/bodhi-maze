﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bodhi
{
    public class Distances
    {
        public Distances(Cell root)
        {
            m_root = root;
            m_cells = new Dictionary<Cell, int>();
            m_cells[m_root] = 0;           
        }

        public void CalculateDistances()
        {
            List<Cell> frontier = new List<Cell>();
            frontier.Add(m_root);

            while (frontier.Count > 0)
            {
                List<Cell> newFrontier = new List<Cell>();

                foreach (var curr in frontier)
                {
                    foreach (var link in curr.Links)
                    {
                        if (m_cells.ContainsKey(link) == true) continue;

                        SetDistance(link, GetDistance(curr) + 1);
                        newFrontier.Add(link);
                    }
                }
                frontier = newFrontier;
            }
        }

        public int GetDistance(Cell cell)
        {
            if (m_cells.ContainsKey(cell) == false)
            {
                return 0;
            }
            return m_cells[cell];
        }

        public void SetDistance(Cell cell, int dist)
        {
            m_cells[cell] = dist;
        }

        public Cell GetFarthestCell(out int distance)
        {
            distance = 0;
            Cell farthest = null;

            foreach (var cell in m_cells)
            {
                if (cell.Value > distance)
                {
                    farthest = cell.Key;
                    distance = cell.Value;
                }
            }

            return farthest;
        }

        public Distances FindPathTo(Cell goal)
        {
            var current = goal;
            var breadcrumbs = new Distances(m_root);

            while (current != m_root)
            {
                foreach (var link in current.Links)
                {
                    if (m_cells[link] < m_cells[current])
                    {
                        breadcrumbs.SetDistance(link, m_cells[link]);
                        current = link;                        
                    }
                }
            }

            return breadcrumbs;
        }

        Dictionary<Cell, int> m_cells;
        private Cell m_root;
    }    
}
