using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace bodhi
{
    /// <summary>
    ///     The grid building the actual maze. A collection of cells
    /// </summary>
    public class Grid
    {
        protected Random m_random;
        protected Cell[,] m_cells;

        public Grid(int width, int height, int seed = 0)
        {
            Width = width;
            Height = height;
            Size = Width*Height;
            Prepare();
            Configure();
            m_random = new Random(seed);
            Generator = "unknown";
        }

        protected Grid()
        {
        }

        public int Width { get; protected set; }
        public int Height { get; protected set; }

        public List<Cell> Deadends
        {
            get
            {
                var result = new List<Cell>();
                for (var y = 0; y < Height; ++y)
                {
                    for (var x = 0; x < Width; ++x)
                    {
                        var cell = m_cells[x, y];
                        if (cell == null) continue;
                        if (cell.Links.Count == 1)
                        {
                            result.Add(cell);
                        }
                    }
                }

                return result;
            }
        }

        public Random Entropy
        {
            get { return m_random; }
        }

        public int Size { get; set; }
        public virtual string Generator { get; set; }

        public List<Cell> Cells
        {
            get
            {
                var result = new List<Cell>(m_cells.Length);
                for (var y = 0; y < Height; ++y)
                {
                    for (var x = 0; x < Width; ++x)
                    {
                        var cell = m_cells[x, y];
                        if (cell == null) continue;
                        result.Add(cell);
                    }
                }
                return result;
            }
        }

        public IEnumerable EachRow()
        {
            for (var y = 0; y < Height; ++y)
            {
                var row = new List<Cell>(Width);
                for (var x = 0; x < Width; ++x)
                {
                    var cell = m_cells[x, y];
                    if (cell == null) continue;
                    row.Add(cell);
                }
                yield return row;
            }
        }

        public IEnumerable EachCell()
        {
            foreach (var cell in m_cells)
            {
                if (cell == null) continue;
                yield return cell;
            }
        }

        public Cell GetCell(int x, int y)
        {
            if (x < 0 || y < 0 || x >= Width || y >= Height) return null;
            return m_cells[x, y];
        }

        public virtual Cell GetRandomCell()
        {
            var x = m_random.Next(0, Width);
            var y = m_random.Next(0, Height);

            var cell = m_cells[x, y];
            
            while (cell == null)
            {
                x = m_random.Next(0, Width);
                y = m_random.Next(0, Height);
                cell = m_cells[x, y];
            }
            Debug.Assert(cell != null);

            return cell;
        }

        private void Prepare()
        {
            m_cells = new Cell[Width, Height];
            for (var y = 0; y < Height; ++y)
            {
                for (var x = 0; x < Width; ++x)
                {
                    m_cells[x, y] = new Cell(x, y);
                }
            }
        }

        protected void Configure()
        {
            for (var y = 0; y < Height; ++y)
            {
                for (var x = 0; x < Width; ++x)
                {
                    var cell = m_cells[x, y];
                    if(cell == null) continue;

                    cell.North = GetCell(x, y + 1);
                    cell.South = GetCell(x, y - 1);
                    cell.West = GetCell(x - 1, y);
                    cell.East = GetCell(x + 1, y);
                }
            }
        }
    }
}