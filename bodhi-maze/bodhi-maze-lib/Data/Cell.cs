using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace bodhi
{
    public class Cell
    {
        [Flags]
        public enum LinkFlag
        {
            None = 0,
            North = 1,
            East = 2,
            West = 4,
            South = 8
        }

        public Cell(int x, int y)
        {
            X = x;
            Y = y;
            
            Links = new List<Cell>();       
            LinkFlags = LinkFlag.None;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
        
        public Cell North { get; set; }
        public Cell South { get; set; }
        public Cell West { get; set; }
        public Cell East { get; set; }

        public List<Cell> Links { get; private set; }

        public LinkFlag LinkFlags  { get; private set; }

        public List<Cell> Neighbors
        {
            get
            {
                var result = new List<Cell>();
                if (North != null) result.Add(North);
                if (South != null) result.Add(South);
                if (West != null) result.Add(West);
                if (East != null) result.Add(East);

                return result;
            }
        }

        public List<Cell> UnlinkedNeighbors        
        {
            get
            {
                var result = new List<Cell>();
                if (North != null && North.IsLinked == false) result.Add(North);
                if (South != null && South.IsLinked == false) result.Add(South);
                if (West != null && West.IsLinked == false) result.Add(West);
                if (East != null && East.IsLinked == false) result.Add(East);
                return result;
            }
        }

        public void Link(Cell cell, bool bidirectional = true)
        {
            if (IsNeighbor(cell) == false) throw new ArgumentException("cell is not a neighbor");

            if (cell == North) LinkFlags |= LinkFlag.North;
            if (cell == South) LinkFlags |= LinkFlag.South;
            if (cell == West) LinkFlags |= LinkFlag.West;
            if (cell == East) LinkFlags |= LinkFlag.East;

            Links.Add(cell);
            if (bidirectional == true)
            {
                cell.Link(this, false);
            }
        }

        public void Unlink(Cell cell, bool bidirectional = true)
        {
            if (IsNeighbor(cell) == false) throw new ArgumentException("cell is not a neighbor");

            if (cell == North) LinkFlags &= ~LinkFlag.North;
            if (cell == South) LinkFlags &= ~LinkFlag.South;
            if (cell == West) LinkFlags &= ~LinkFlag.West;
            if (cell == East) LinkFlags &= ~LinkFlag.East;

            bool removed = Links.Remove(cell);
            if (bidirectional == true)
            {
                cell.Unlink(this, false);
            }
            Debug.Assert(removed);
        }

        public bool IsLinkedWith(Cell cell)
        {
            if (cell == null) return false;
            if (IsNeighbor(cell) == false) throw new ArgumentException("cell is not a neighbor");
            
            return Links.Contains(cell);
        }

        public bool IsLinked
        {
            get { return Links.Count > 0; }
        }

        public bool IsNeighbor(Cell cell)
        {
            if (cell == null) return false;

            if (cell == North) return true;
            if(cell == South) return true;
            if (cell == West) return true;
            if (cell == East) return true;
            return false;
        }
    }
}
