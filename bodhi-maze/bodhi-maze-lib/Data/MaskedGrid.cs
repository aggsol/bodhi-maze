﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace bodhi.Data
{
    public class MaskedGrid : Grid
    {
        private Bitmap m_mask;
        private string m_generator;

        public MaskedGrid(string png, int seed = 0)
        {
            m_mask = Image.FromFile(png, true) as Bitmap;
            Width = m_mask.Width;
            Height = m_mask.Height;
            Size = Width * Height;
            m_random = new Random(seed);
            Generator = "unknown";

            Prepare();
            Configure();
        }

        public override string Generator
        {
            get { return m_generator; }
            set { m_generator = "Masked"+value; }
        }

        private void Prepare()
        {
            m_cells = new Cell[Width, Height];
            for (var y = 0; y < Height; ++y)
            {
                for (var x = 0; x < Width; ++x)
                {
                    var pixel = m_mask.GetPixel(x, Height - y - 1);
                    if (pixel.R > 0)
                    {
                        m_cells[x, y] = new Cell(x, y);
                    }
                }
            }
        }
    }
}
