﻿using System;
using System.Collections.Generic;

namespace bodhi.Generator
{
    public static class Wilson
    {
        public static void Run(Grid grid, Random rand = null)
        {
            if (grid == null) throw new ArgumentNullException("grid");
            if (rand == null) rand = grid.Entropy;

            grid.Generator = "Wilson";

            var unvisited = grid.Cells;
            var first = unvisited.GetRandomSample(rand);
            unvisited.Remove(first);

            while (unvisited.Count > 0)
            {
                var cell = unvisited.GetRandomSample(rand);
                var path = new List<Cell>();
                path.Add(cell);

                while (unvisited.Contains(cell))
                {
                    cell = cell.Neighbors.GetRandomSample(rand);
                    var pos = path.IndexOf(cell);
                    if (pos >= 0)
                    {
                        path.RemoveRange(pos + 1, path.Count - pos - 1);
                    }
                    else
                    {
                        path.Add(cell);
                    }
                }

                for (var i = 0; i < path.Count - 1; ++i)
                {
                    path[i].Link(path[i + 1]);
                    unvisited.Remove(path[i]);
                }
            }
        }
    }
}