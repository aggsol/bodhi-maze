﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bodhi.Generator
{
    public class AldousBroder
    {
        public static void Run(Grid grid, Random rand = null)
        {
            if(grid == null) throw new ArgumentNullException("grid");
            if (rand == null) rand = grid.Entropy;

            grid.Generator = "AldousBroder";
            int unvisited = grid.Size - 1;
            var cell = grid.GetRandomCell();

            while (unvisited > 0)
            {
                var neighbor = cell.Neighbors.GetRandomSample(rand);

                if (neighbor.Links.Count == 0)
                {
                    cell.Link(neighbor);
                    unvisited--;
                }

                cell = neighbor;
            }
        }
    }
}
