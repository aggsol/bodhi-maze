﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bodhi.Generator
{
    public static class RecursiveBacktracker
    {
        public static void Run(Grid grid, Random rand = null)
        {
            if(grid == null) throw new ArgumentNullException("grid");
            if (rand == null) rand = grid.Entropy;
            grid.Generator = "RecursiveBacktracker";

            var stack = new Stack<Cell>();
            var start = grid.GetRandomCell();
            stack.Push(start);

            while (stack.Count > 0)
            {
                Cell curr = stack.Peek();
                var neighbors = curr.UnlinkedNeighbors;

                if (neighbors.Count == 0)
                {
                    stack.Pop();
                }
                else
                {
                    var neighbor = neighbors.GetRandomSample(rand);
                    curr.Link(neighbor);
                    stack.Push(neighbor);
                }
            }
        }
    }
}
