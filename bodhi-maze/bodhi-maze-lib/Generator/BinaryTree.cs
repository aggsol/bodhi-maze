﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bodhi
{
    public static class BinaryTree
    {
        public static void RunNorthEast(Grid grid, Random rand = null)
        {
            if(grid == null) throw new ArgumentNullException("grid");
            if (rand == null) rand = grid.Entropy;

            grid.Generator = "BinaryTreeNorthEast";
            Random random = new Random(1);
            foreach (Cell cell in grid.EachCell())
            {
                List<Cell> cells = new List<Cell>();

                if (cell.North != null) cells.Add(cell.North);
                if (cell.East != null) cells.Add(cell.East);

                if (cells.Count == 0) continue;

                var candidate = cells.GetRandomSample(rand);
                cell.Link(candidate, true);
            }
        }

        public static void RunSouthWest(Grid grid, Random rand = null)
        {
            if (grid == null) throw new ArgumentNullException("grid");
            if (rand == null) rand = grid.Entropy;

            grid.Generator = "BinaryTreeSouthWest";
            
            foreach (Cell cell in grid.EachCell())
            {
                List<Cell> cells = new List<Cell>();

                if (cell.South != null) cells.Add(cell.South);
                if (cell.West != null) cells.Add(cell.West);

                if (cells.Count == 0) continue;

                var candidate = cells.GetRandomSample(rand);
                cell.Link(candidate, true);
            }
        }

    }
}
