﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bodhi
{
    public static class SideWinder
    {
        static public void Run(Grid grid, Random random = null)
        {
            if(grid == null) throw new ArgumentNullException("grid");
            grid.Generator = "SideWinder";
            if (random == null)
            {
                random = grid.Entropy;
            }

            foreach (List<Cell> row in grid.EachRow())
            {
                var run = new List<Cell>();

                foreach (var cell in row)
                {
                    run.Add(cell);

                    bool atEastBoundary = (cell.East == null);
                    bool atNorthBoundary = (cell.North == null);
                    bool closeOut = atEastBoundary
                                    || (!atNorthBoundary && (random.Next(2) == 0));

                    if (closeOut == true)
                    {
                        var member = run.GetRandomSample(random);
                        if (member.North != null)
                        {
                            member.Link(member.North);
                            run.Clear();
                        }
                    }
                    else
                    {
                        cell.Link(cell.East);
                    }
                }
            }
        }
    }
}
