﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bodhi.Generator
{
    public static class HuntAndKill
    {
        public static void Run(Grid grid, Random rand = null)
        {
            if(grid == null) throw new ArgumentNullException("grid");
            grid.Generator = "HuntAndKill";
            if (rand == null) rand = grid.Entropy;

            var curr = grid.GetRandomCell();

            while (curr != null)
            {
                var unvisitedNeighbors = curr.UnlinkedNeighbors;
                if (unvisitedNeighbors.Count > 0) // random walk (killing cells)
                {
                    var neighbor = unvisitedNeighbors.GetRandomSample(rand);
                    curr.Link(neighbor);
                    curr = neighbor;
                }
                else // Hunt for next cell
                {
                    curr = null;

                    foreach (Cell cell in grid.EachCell())
                    {
                        var links = cell.Links;
                        var visitedNeighbors = (from n in cell.Neighbors
                                                where n.IsLinked == true
                                                select n).
                            ToList();

                        if (links.Count == 0 && visitedNeighbors.Count > 0)
                        {
                            curr = cell;
                            var neighbor = visitedNeighbors.GetRandomSample(rand);
                            curr.Link(neighbor);
                            break;
                        }
                    }
                }
            }
        }
    }
}
