using NUnit.Framework;
using System;
using System.Collections.Generic;
using bodhi;

namespace bodhimazetest
{
	[TestFixture]
	public class CellTest
	{
	    [Test]
	    public void TestNeighbour()
	    {
	        var curr = new Cell(0, 0);

            Assert.IsFalse(curr.IsNeighbor(null));

            var north = new Cell(0, 1);

            Assert.IsFalse(curr.IsNeighbor(north));
	        curr.North = north;

            Assert.IsTrue(curr.IsNeighbor(north));
	    }

	    [Test]
	    public void TestFlags()
	    {
            var curr = new Cell(0, 0);

            Assert.AreEqual(Cell.LinkFlag.None, curr.LinkFlags);

            var north = new Cell(0, 1);
            curr.North = north;
            north.South = curr;
            curr.Link(north);

            Assert.AreEqual(Cell.LinkFlag.North, curr.LinkFlags);

            var west = new Cell(-1, 0);
            curr.West = west;
            west.East = curr;
            west.Link(curr);

            Assert.AreEqual(Cell.LinkFlag.North | Cell.LinkFlag.West, curr.LinkFlags);

            curr.Unlink(north);

            Assert.AreEqual(Cell.LinkFlag.West, curr.LinkFlags);
	    }


		[Test]
		public void TestUnlinkedNeighbors ()
		{
            var curr = new Cell(0, 0);

            Assert.AreEqual(0, curr.UnlinkedNeighbors.Count);
            Assert.IsFalse(curr.IsLinked);

            var north = new Cell(0, 1);
		    curr.North = north;
		    north.South = curr;

            Assert.AreEqual(1, curr.UnlinkedNeighbors.Count);
            Assert.IsFalse(curr.IsLinked);
            Assert.IsFalse(curr.IsLinkedWith(north));

            var south = new Cell(0, -1);
		    curr.South = south;
		    south.North = curr;

            Assert.AreEqual(2, curr.UnlinkedNeighbors.Count);
            Assert.IsFalse(curr.IsLinked);

            curr.Link(north);

            Assert.AreEqual(1, curr.UnlinkedNeighbors.Count);
            Assert.IsTrue(curr.IsLinked);
            Assert.IsTrue(curr.IsLinkedWith(north));
            Assert.IsFalse(curr.IsLinkedWith(south));

            curr.Link(curr.South);

            Assert.AreEqual(0, curr.UnlinkedNeighbors.Count);
            Assert.IsTrue(curr.IsLinkedWith(north));
            Assert.IsTrue(curr.IsLinkedWith(south));

		}
	}
}

