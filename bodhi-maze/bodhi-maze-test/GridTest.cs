using NUnit.Framework;
using System;
using System.Collections.Generic;
using bodhi;

namespace bodhimazetest
{
	[TestFixture]
	public class GridTest
	{
		[Test]
		public void EachRow ()
		{
			Grid grid = new Grid(4, 5, 6);
		    int counter = 0;
			foreach (List<Cell> row in grid.EachRow())
			{
				Assert.AreEqual (4, row.Count);
			    counter++;
			}
            Assert.AreEqual(5, counter);
		}

	    [Test]
	    public void EachCell()
	    {
	        Grid grid = new Grid(8, 7, 6);
	        int counter = 0;
	        foreach (var cell in grid.EachCell())
	        {
	            counter++;
	        }
            Assert.AreEqual(8*7, counter);
            Assert.AreEqual(grid.Size, counter);
	    }
	}
}

