using NUnit.Framework;
using System;
using System.Collections.Generic;
using bodhi;
using bodhi.Generator;

namespace bodhimazetest
{
	[TestFixture]
	public class WillsonTest
	{
		[Test]
		public void GenerateSquares()
		{
		    for (int i = 1; i < 25; i+=5)
		    {
		        Grid grid = new Grid(i, i, i);
                Wilson.Run(grid);
		    }
		}

	    [Test]
	    public void GeneratStrips()
	    {
            for (int i = 3; i < 10; i += 3)
            {
                Grid grid = new Grid(i, i * i, i + 1);
                Wilson.Run(grid);
            }
            
            for (int i = 3; i < 10; i += 3)
            {
                Grid grid = new Grid(i*i, i, i + i);
                Wilson.Run(grid);
            }
        }
	}
}

