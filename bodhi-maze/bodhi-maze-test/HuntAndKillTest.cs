using bodhi;
using bodhi.Generator;
using NUnit.Framework;

namespace bodhimazetest
{
	[TestFixture]
	public class HuntAndKillTest
	{
		[Test]
		public void GenerateSquares()
		{
		    for (int i = 1; i < 25; i+=5)
		    {
		        Grid grid = new Grid(i, i, i);
                HuntAndKill.Run(grid);
		    }
		}

	    [Test]
	    public void GeneratStrips()
	    {
            for (int i = 3; i < 10; i += 3)
            {
                Grid grid = new Grid(i, i * i, i + 1);
                HuntAndKill.Run(grid);
            }
            
            for (int i = 3; i < 10; i += 3)
            {
                Grid grid = new Grid(i*i, i, i + i);
                HuntAndKill.Run(grid);
            }
        }
	}
}

