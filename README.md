# Bodhi Maze

C# implementation of some algorithms of Jamis Buck's book "Mazes for Programmers" 
(http://www.mazesforprogrammers.com). It also allows the export of created mazes 
for Tiled (http://www.mapeditor.org) 

## How To Build

Solution file for Monodevelop and Visual Studio is included.

### bodhi-maze-lib

Implementation of the algorithms

### bodhi-maze-cli

Command line interface that creates PNG and TMX files

### bodhi-maze-test

Some unit tests.

## License

GPL v3